const { MessageEmbed } = require("discord.js");

exports.use = async (client, message, args, command) => {
    if (!message.member.voice.channel) return message.reply("You have to be in a voice channel to have me join ya!");
    message.member.voice.channel.join();
    message.reply("I have joind ya!")
};

exports.command = {
    aliases: ["join"]
};