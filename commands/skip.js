const { active } = require("../utils/variables");
exports.use = async (client, message, args, command, link) => {
  if (!message.member.voice.channel) return message.reply("You have to be in a voice channel to play a song!");
  if (message.member.voice.channelID !== message.guild.me.voice.channelID) return message.reply("We are not in the same channel!");
  let fetched = active.get(message.guild.id);
  if (!fetched) return message.reply("There isn't any songs playing at this moment!");

  let userCount = message.member.voice.channel.members.size;
  let required = Math.ceil(userCount / 2);

  if (!fetched.queue[0].voteSkip) fetched.queue[0].voteSkip = [];
  if (fetched.queue[0].voteSkip.includes(message.member.id)) return message.reply(`You cant vote to skip twice! **${fetched.queue[0].voteSkip.length}/${required}** required votes.`);

  fetched.queue[0].voteSkip.push(message.member.id);

  active.set(message.guild.id, fetched);

  if (fetched.queue[0].voteSkip.length >= required) {
    message.reply("Song skipped!");
    fetched.dispatcher.emit("finish");
  }

  message.reply(`You have voted to skip! **${fetched.queue[0].voteSkip.length}/${required}** required votes.`);

};

exports.command = {
  aliases: [""]
};	