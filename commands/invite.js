const { MessageEmbed } = require("discord.js");
const { guilds } = require("../utils/variables");

exports.use = async (client, message, args, command) => {

    let inviteEmbed = new MessageEmbed()
        .setColor(guilds.get(message.guild.id).embedColor)
        .setTitle("Yammy - Invite")
        .setDescription("It is suggested to invite the bot with administrator permissions in order to bypass all permissions checks. This means that you only have to move the role over the role you are giving to make it work. Or you could use the normal invite that should have all needed permissions but does not bypass channel and other permissions.")
        .addField("Normal Invite", "You can invite Yammy by clicking [here](https://discordapp.com/api/oauth2/authorize?client_id=585103578742652947&permissions=36785408&scope=bot) in order to only give it needed permissions.")
        .setURL("https://discordapp.com/api/oauth2/authorize?client_id=585103578742652947&permissions=36785408&scope=bot")
        .setFooter(guilds.get(message.guild.id).footer);

    return message.channel.send(inviteEmbed).catch(err => { });
};

exports.command = {
    aliases: [""]
}