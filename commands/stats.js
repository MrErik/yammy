const { MessageEmbed } = require("discord.js");
const { guilds } = require("../utils/variables");

exports.use = async (client, message, args, command) => {
    const promises = [
        client.shard.fetchClientValues('guilds.size'),
        client.shard.broadcastEval('this.guilds.reduce((prev, guild) => prev + guild.memberCount, 0)'),
        client.shard.fetchClientValues('channels.size'),
        client.shard.broadcastEval('process.memoryUsage().heapUsed')
    ];
    Promise.all(promises).then(results => {
        const totalGuilds = results[0].reduce((prev, guildCount) => prev + guildCount, 0);
        const totalMembers = results[1].reduce((prev, memberCount) => prev + memberCount, 0);
        const totalChannels = results[2].reduce((prev, channelCount) => prev + channelCount, 0);
        let totalHeap = results[3].reduce((prev, heap) => prev + heap, 0);
        totalHeap = totalHeap / 1024 / 1024
        const embedStats = new MessageEmbed()
            .setTitle("Yammy - Statistics")
            .setColor(guilds.get(message.guild.id).embedColor)
            .setFooter(guilds.get(message.guild.id).footer)
            .addField("• Ram Usage", `${Math.round(totalHeap / 1024 * 100) / 100}GB`)
            .addField("• Users", totalMembers)
            .addField("• Servers", totalGuilds)
            .addField("• Channels", totalChannels)
            .addField("• API Latency", `${Math.round(client.ws.ping)} ms`)
            .addField("• Shard", client.shard.id);

        message.channel.send(embedStats).catch(err => { });
    });
};


exports.command = {
    aliases: [""]
}