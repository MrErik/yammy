exports.use = async (client, message, args, command, link) => {
    if (!message.member.voice.channel) return message.reply("You have to be in a voice channel to play a song!");
    if (!message.guild.me.voice.channel) return message.reply("I can't leave a voice channel that I am not in?");
    if (message.member.voice.channelID !== message.guild.me.voice.channelID) return message.reply("We are not in the same channel!");
    message.guild.me.voice.channel.leave();
    message.reply("I am leaving the channel...");
};

exports.command = {
    aliases: [""]
};	