const { MessageEmbed } = require("discord.js");
const search = require("yt-search");
exports.use = async (client, message, args, command) => {

    search(args.join(" "), function (err, res) {
        if (err) return message.reply("Something went wrong, try again.");

        let videos = res.videos.slice(0, 10);

        let resp = '';
        for (let i in videos) {
            resp += `**[${parseInt(i) + 1}]:** \`${videos[i].title}\`\n`;
        }
        resp += `\nChoose a number between \`1-${videos.length}\``;
        message.channel.send(resp);

        const filter = m => !isNaN(m.content) && m.content < videos.length + 1 && m.content > 0 && m.author.id == message.author.id;
        const collector = message.channel.createMessageCollector(filter, { time: 60000 });

        collector.videos = videos;
        collector.once("collect", function (m) {
            let commandFile = require("./play");
            commandFile.use(client, message, [this.videos[parseInt(m.content) - 1].url], command);
        });
        collector.once("error", () => message.reply("You did not choose a number within 5 minutes! Canceling request!"));
    });

};

exports.command = {
    aliases: [""]
};